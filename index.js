
const fs      = require('fs')
const moment  = require('moment');
const Promise = require('bluebird');
const _       = require('lodash');


const env = require('./env');
const HelpersDataBase = require('./HelpersDataBase');
const logger          = require('./logConfig');
const utils           = require('./utils');

const tmpDir  = 'tmp';

//create folder for tmp not exists.
if (!fs.existsSync(tmpDir)) fs.mkdirSync(tmpDir);

function asyncLoopsChunkCollections( dbBackup, dbRestore, collections, len) {
    return new Promise(async (resolve, reject) => {

        let resultsChunk = [], cursor, doc, collection, count, countTotal, porcentages, porcentageTotal, startDate, collectionRestore, indexes;
        const collectionFsFilesBackup = await dbBackup.collection('fs.files')
        const collectionFsFilesRestore = await dbRestore.collection('fs.files')
        async function helperChunk(i, cb) {
            try{

                if (i == len) {

                    cursor = doc = collection = count = countTotal = porcentages = porcentageTotal = startDate = collectionRestore, indexes = null;
                    return cb(null, resultsChunk);
                }

                startDate   = new Date();
                collection  = collections[i];
                count       = 0;
                porcentages = utils.generateArrayPorcentage('.', 25);
                porcentageTotal = '0.0';

                logger.info( `${new Date().toISOString()}  [${porcentages.join('')}] ${dbBackup.databaseName}.${collection.collectionName}  (${porcentageTotal}%)` );

                countTotal  = await collection.countDocuments({});

                try{ await dbRestore.collection(String(collection.collectionName)).drop(); }catch(err){}

                collectionRestore = await dbRestore.collection(String(collection.collectionName));

                indexes = await collection.indexes();

                indexes.forEach( index => {
                    delete index.v;
                    delete index.ns;
                    let key = index.key;
                    delete index.key;
                    let options = [];
                    for (let option in index) {
                        options.push(index[option]);
                    }
                    collectionRestore.createIndex(key, options);
                });

                let opts = {
                    cursor: {},
                    batchSize:10,
                    maxTimeMS: 1000,
                    // allowDiskUse: true
                };

                cursor = await collection.aggregate([], opts);

                while (doc = await cursor.next()) {

                    await collectionRestore.insertOne(doc);

                    if(doc.pdf){
                        const file = await collectionFsFilesBackup.findOne({filename: doc.pdf})
                        if( file ){

                        }
                    }

                    count++;
                    let index       = parseInt( (count * 25) / countTotal )
                    porcentageTotal = parseFloat( (count * 100) / countTotal ).toFixed(1)

                    porcentages = utils.generateArrayPorcentage('#', index, porcentages);

                    logger.info( `${new Date().toISOString()}  [${porcentages.join('')}] ${dbBackup.databaseName}.${collection.collectionName}  (${porcentageTotal}%)` );

                }

                logger.info( `${new Date().toISOString()}  finished backup and restoring ${dbBackup.databaseName}.${collection.collectionName}  (${countTotal} documents)` );

                resultsChunk.push({
                    startDate : startDate,
                    endDate   : new Date(),
                    collectionName : collection.collectionName,
                    documents      : countTotal
                });

                // setTimeout(helperChunk.bind(null, i + 1, cb), 1500);
                setImmediate(helperChunk.bind(null, i + 1, cb));

            }catch (err){

                console.log('*************************')
                console.log('asyncLoopsChunk > helperChunk ERROR')
                console.log(err)

                cb(err);

            }

        }

        // Start the helper.
        helperChunk(0, function(error, results){

            if(error) return reject(error);
            resolve(results);

        });

    })

}

async function startBackupAndRestore() {

    try{

        if( !env ) throw new Error('The env.json file does not exist, copy env.example.json and replace the data.');
        if( !env.active ) throw new Error('You must activate the script in env.json, {active: true}');

        const mongoBackup  = new HelpersDataBase(env.serverBackup, {});
        const mongoRestore = new HelpersDataBase(env.serverRestore);

        const dbBackup  = await mongoBackup._connect();
        const dbRestore = await mongoRestore._connect();

        if( !dbBackup )  throw new Error("We couldn't connect to mongo Backup");
        if( !dbRestore ) throw new Error("We could not connect to mongo Restore");

        let label = `backup_${dbBackup.databaseName}`;
        let startDate = new Date();

        logger.info( `${startDate.toISOString()} - Connection mongo for backup, we\'re connected!` );
        logger.info( `${startDate.toISOString()} - Connection mongo for restore, we\'re connected!` );
        logger.info( `${startDate.toISOString()} - dbBackup open: ${dbBackup.databaseName}` );
        logger.info( `${startDate.toISOString()} - dbRestore open: ${dbRestore.databaseName}` );

        console.time(label)

        dbBackup.collections((err, collections) =>{

            if(err) throw err

            let last = ~~collections.length;

            if (last === 0) throw new Error('empty set');

            let filterCollections = [];
            let fsCollections = [];
            let omitCollections   = env.serverBackup.omitCollections.length > 0 ? env.serverBackup.omitCollections.concat(['fs.files', 'fs.chunks']) : ['fs.files', 'fs.chunks'];

            collections.forEach(c => {

                if( !omitCollections.includes(c.collectionName) ) filterCollections.push(c);
                if( ['fs.files', 'fs.chunks'].includes(c.collectionName) ) fsCollections.push(c);

            });

            filterCollections = _.orderBy(filterCollections, ['collectionName'], ['asc']);

            asyncLoopsChunkCollections(dbBackup, dbRestore, filterCollections, filterCollections.length)
                .then(async results =>{

                    let fsFilesErrors = [], timeEnd;
                    if( fsCollections.length > 0 ){

                        let file;
                        const _startDate    = new Date();
                        const fsFilesCollection  = fsCollections.find(c => c.collectionName == 'fs.files')
                        const fsChunksCollection  = fsCollections.find(c => c.collectionName == 'fs.chunks')
                        const bucketBackup  = await mongoBackup._getBucket();
                        const bucketRestore = await mongoRestore._getBucket();
                        try { await bucketRestore.drop() }catch (e) {}

                        let countTotal  = await mongoBackup.getBucketSize(),
                            count       = 0, porcentageTotal = '0.0',
                            porcentages = utils.generateArrayPorcentage('.', 25);

                        logger.info( `${new Date().toISOString()}  [${porcentages.join('')}] ${dbBackup.databaseName}.${fsFilesCollection.collectionName} ${utils.parserSizesFiles(countTotal, 2)}  (${porcentageTotal}%)` );

                        const cursor = await bucketBackup.find({}).batchSize(2).maxAwaitTimeMS(1000 * 60);

                        while (file = await cursor.next() ){
                            await receiveData(file)
                        }
                        await receiveDataEnd();

                        async function receiveData(file){
                            const ops = {
                                contentType : file.contentType,
                                length      : file.length,
                                chunkSize   : file.chunkSize,
                                aliases     : file.aliases,
                                metadata    : file.metadata
                            };
                            try{

                                const buff = await mongoBackup.getFileData(file.filename, dbBackup.databaseName, file.length)
                                await mongoRestore.createFileWithId(file._id, file.filename, ops, buff);

                                count += file.length;
                                let index       = parseInt( (count * 25) / countTotal )
                                porcentageTotal = parseFloat( (count * 100) / countTotal ).toFixed(1)

                                porcentages = utils.generateArrayPorcentage('#', index + 1, porcentages);

                                logger.info( `${new Date().toISOString()}  [${porcentages.join('')}] ${dbBackup.databaseName}.${fsFilesCollection.collectionName} ${utils.parserSizesFiles(count,2)}/${utils.parserSizesFiles(countTotal,1)} (${porcentageTotal}%)` );

                            }catch (err) {

                                fsFilesErrors.push({
                                    date           : new Date(),
                                    collectionId   : file._id,
                                    collectionName : fsFilesCollection.collectionName,
                                    error          : logger.parseError(err)
                                })

                            }
                        }

                        async function receiveDataEnd(){

                            filterCollections.push( fsFilesCollection );
                            filterCollections.push( fsChunksCollection );

                            results.push( {
                                startDate : _startDate,
                                endDate   : new Date(),
                                collectionName : fsFilesCollection.collectionName,
                                chunkSizes     : utils.parserSizesFiles(countTotal,1)
                            });

                            results.push( {
                                startDate : _startDate,
                                endDate   : new Date(),
                                collectionName : fsChunksCollection.collectionName,
                                chunkSizes     : utils.parserSizesFiles(countTotal,1)
                            })

                            finalize()

                        }

                    }else {

                        finalize()

                    }

                    function finalize () {

                        try{

                            let endDate = new Date();

                            timeEnd = `${moment(endDate).diff(moment(startDate), 'm')} minutes`;

                            let dataJSON = JSON.stringify({
                                "startDate" : startDate,
                                "endDate"   : endDate,
                                "timeEnd": `${timeEnd} minutes`,
                                "dbBackupName"  : dbBackup.databaseName,
                                "dbRestoreName" : dbRestore.databaseName,
                                "collections"   : filterCollections.length,
                                "results"       : results,
                                "fsFilesErrors": fsFilesErrors
                            }, null, 2);

                            fs.writeFileSync(`${process.cwd()}/tmp/backupAndRestore_${moment(endDate).format('YYYY_MM_DD_HH_MM_SS')}.json`, dataJSON)

                        }catch (err) {
                            logger.error( `index.js - fs.writeFileSync() -  ${logger.parseError(err)}` );
                        }

                        logger.info( `collections: ${filterCollections.length}, Backup: ${results.length}, fsFilesErrors: ${fsFilesErrors.length} timeEnd: ${timeEnd}` );
                        console.timeEnd(label)

                    }

                }).catch(err =>{
                    console.timeEnd(label)
                    throw err;
            })

        })

    }catch (err) {
        logger.error( `index.js - init() -  ${logger.parseError(err)}` );
    }
}

setTimeout(startBackupAndRestore, 1500);

