const fs           = require('fs')
const mongodb      = require('mongodb');
const GridFSBucket = require('mongodb').GridFSBucket;
const tunnel       = require('tunnel-ssh');
const logger       = require('./logConfig');
const utils         = require('./utils');

class HelpersDataBase {

	constructor(config, mongoOptions={}){

		this._tunnelConfig = {
			...config,
			agent : process.env.SSH_AUTH_SOCK,
			privateKey: fs.readFileSync('./ca.pem'),
			keepAlive:true
		};
		this._useTunnelSSH = config.useTunnelSSH;
		this._databaseURI  = config.databaseURI.toString();

		const defaultMongoOptions = {
			// autoConnect : config.useTunnelSSH ? false : true,
			useNewUrlParser: true,
			useUnifiedTopology: true,
			connectTimeoutMS: 600000
		};
		this._mongoOptions = Object.assign(defaultMongoOptions, mongoOptions);
	}

	async _connect() {
		if (!this._connectionPromise) {
			if( this._useTunnelSSH && !this._sshTunnel ){
				this._sshTunnel = await this._tunnelPromise(this._tunnelConfig);
			}
			this._connectionPromise = mongodb.MongoClient.connect(
				this._databaseURI,
				this._mongoOptions
			).then(client => {
				this._client = client;
				return client.db(client.s.options.dbName);
			});

		}
		return this._connectionPromise;
	}

	_tunnelPromise(config){
		return new Promise((resolve, reject) => {
			tunnel(config, (error, server) => {
				if (error) {
					logger.error( `helpersDataBase.js - _tunnelPromise() -  ${logger.parseError(err)}` );
					reject(error);
				} else {
					resolve(server);
				}
			});
		});
	}

	async close() {
		const db = await this._connect();
		if(db) db.close();
		if(this._sshTunnel)this._sshTunnel.close();
	}

	_getBucket() {
		return this._connect().then(database => new GridFSBucket(database));
	}

	async createFile(filename, data) {
		const bucket = await this._getBucket();
		const stream = await bucket.openUploadStream(filename);
		await stream.write(data);
		stream.end();
		return new Promise((resolve, reject) => {
			stream.on('finish', resolve);
			stream.on('error', reject);
		});
	}

	async createFileWithId(_id, filename, opts, data) {
		const bucket = await this._getBucket();
		const stream = await bucket.openUploadStreamWithId(_id, filename, opts);
		await stream.write(data);
		stream.end();
		return new Promise((resolve, reject) => {
			stream.on('finish', resolve);
			stream.on('error', reject);
		});
	}

	async getFileData(filename, databaseName, lengthTotal) {
		const bucket = await this._getBucket();
		const stream = bucket.openDownloadStreamByName(filename, { timeout: true});
		stream.read();
		return new Promise((resolve, reject) => {
			const chunks = [];
			let length = 0, porcentages = utils.generateArrayPorcentage('.', 25);

			stream.on('data', data => {
				chunks.push(data);
				length += Buffer.byteLength(data);
				let index       = parseInt( (length * 25) / lengthTotal )
				let porcentageTotal = parseFloat( (length * 100) / lengthTotal ).toFixed(1)
				porcentages = utils.generateArrayPorcentage('#', index, porcentages);

				logger.info( `${new Date().toISOString()}  [${porcentages.join('')}] ${databaseName}.fs.chunks ${ utils.parserSizesFiles(length, 2)}/${utils.parserSizesFiles(lengthTotal,1)} (${porcentageTotal}%)` );

			});
			stream.on('end', () => {
				resolve(Buffer.concat(chunks));
			});
			stream.on('error', err => {
				reject(err);
			});
		});
	}

	async getBucketSize() {
		return new Promise(async (resolve, reject) => {
			try {

				const db = await this._connect();

				const collection = await db.collection(String('fs.files'));
				const p = [{
					"$group": {
						"_id" : null,
						"length":{
							"$sum" : "$length"
						}
					}
				}];

				let length = 0;
				const results = await collection.aggregate(p).toArray();
				if(results.length > 0) length = results[0].length
				resolve(length)
			}catch (e) {
				reject(e)
			}
		})


	}


};

module.exports = HelpersDataBase;
