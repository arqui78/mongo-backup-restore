const fs    = require('fs');
const winston = require('winston');
const moment  = require('moment');

var logDir  = 'logs';
var prefix  = 'script-mongod-backup';

//create folder for logs if not exists
if (!fs.existsSync(logDir)) fs.mkdirSync(logDir);

winston.add(require('winston-daily-rotate-file'), {
	name     : 'mainlog',
	level    : "info",
	filename : logDir + '/' + prefix + '_log_info.log',
	maxsize  : 1024 * 1024 * 10,
	maxFiles : 2
});

var logger = new(winston.Logger)({

	transports: [
		new(winston.transports.File)({
			level    : 'info',
			name     : 'info-file',
			filename : logDir + '/' + prefix + '_info_' + moment().format('YYYY-MM-DD')  + '.log',
			handleExceptions: true,
			maxsize: 5242880, // 5MB
			//maxsize  : 1024 * 1024 * 10,
			maxFiles : 5,
    		json: true,
    		colorize: false,
		}),
		new(winston.transports.File)({
			level    : 'error',
			name     : 'error-file',
			filename : logDir + '/' + prefix + '_err_' + moment().format('YYYY-MM-DD') + '.log',
			maxsize: 5242880, // 5MB
			// maxsize  : 1024 * 1024 * 10,
			maxFiles : 2
		}),
		new(winston.transports.Console)({
			level: 'debug',
			handleExceptions: true,
			json: false,
			colorize    : true,
			prettyPrint : true
		})
	],
	exitOnError: false, // do not exit on handled exceptions
});

logger.parseError = function (error) {

	if(typeof error == 'string') return error;
	if(typeof error == 'object') return require('util').inspect(error, {depth: null})
	return error
}

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
	write: function(message, encoding) {
	  // use the 'info' log level so the output will be picked up by both transports (file and console)
	  logger.info(message);
	},
  };

module.exports = logger;
