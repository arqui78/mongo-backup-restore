
# Mongo Duplicate
Backup a remote DB and restore immediately in the process. The connection can be direct through authentication or via ssh.

### What do you need to execute this app on your computer?

-   [node.js] - evented I/O for the backend

```sh
curl -sL https://deb.nodesource.com/setup_9.x | apt-get install -y nodejs

apt-get install -y build-essential
```

### Installation

first clone the repository

```sh
$ git clone git@gitlab.com:arqui78/mongo-backup-restore.git
```

enter to cloned folder

```sh
 cd mongo-backup-restore
```

### How to run it

-   [npm] - First you need to install all required node modules, so execute this

```sh
$ npm install
```

### ENV

Copy the env.example.json and save as env.json and edit the values to the respective keys:

# Running the app

## Production

### Compile for production:

I completed your files before uploading to production

```sh
$ npm run start
```

### For developers run with this:

```sh
$  npm run dev
```

[node.js]: http://nodejs.org
[npm]: https://www.npmjs.com/
