
const HelpersDataBase = require('./HelpersDataBase');

const mongoConfig = {
    "databaseName":"test",
    "useTunnelSSH": false,
    "databaseURI": "mongodb://localhost:27017/test",
}

describe('Mongo Backup-Restore', () => {
    test('should return connect DB Test', async () => {
        // expect(true).toBe(true);
        try {
            const helpersDataBase = new HelpersDataBase(mongoConfig, {});
            const db = await helpersDataBase._connect();
            expect('test').toBe(db.databaseName);
        } catch (e) {
            expect(e).toMatch('error');
        }


    });
});