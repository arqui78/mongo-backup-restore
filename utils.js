
module.exports = {
    generateArrayPorcentage: generateArrayPorcentage,
    parserSizesFiles:parserSizesFiles
}

function generateArrayPorcentage(value, len, arry) {

    if( arry ){

        for (let i = 0; i < len; i++){
            arry[i] = value
        }

    }else{

        arry = [];

        for (let i = 0; i < len; i++){
            arry.push(value)
        }

    }

    return arry;

}

function parserSizesFiles(size, fractionDigits) {
    let unity = 'KB', _fractionDigits = fractionDigits || 0;
    size = size / 1024;
    if( size > 1024) {
        size = size / 1024;
        unity = 'MB';
        if( size > 1024) {
            size = size / 1024;
            unity = 'GB';
        }
    }

    return (`${parseFloat(size).toFixed(_fractionDigits)}${unity}`)

}